# Score-Glider

Score Glider!

A little command line based game that's intended to be cross-platform. It's about answering a random number range being even or odd within a certain amount of time (3 seconds in this case), building enough score that leads to random facts and trivia. I tried to make it modular, allowing users to change what want through variables. To play the game execute the file within the "Score-Glider" directory using the command `python3 score-glider.py`. For more information such as licensing, it is in the python file.

## Installation

### Dependencies
1. To install the game, the pygame and randfacts libraries should be installed with `pip install pygame randfacts` in the terminal. 
2. Libraries used in the program such as threading, time, and random are standard python libraries that are included by default, so there's no need to install them manually :)

### Git Cloning and Launching the Program
1. `git clone https://codeberg.org/Sensitive-Sky/Score-Glider` or you can download the repository with other methods.
2. Go to the "Score-Glider" directory.
3. Then launch "score-glider.py" within the "Score-Glider" directory using python3 in the terminal.