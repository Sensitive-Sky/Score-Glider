# Score Glider!
# This game is about giving the player a number and have to enter if that number is even or odd under a certain amount of time. The more correct responses, the more score the player earns eventually reaching milestones that give the player random facts to read.

# Dependencies are pygame, randfacts, threading, time, and random libraries for Python 3

# Imports

# The time library is for its `time.sleep` function for the start text.
import time

# This library is for time limit for each question, using the `Timer` function.
from threading import Timer

# The random library is for generating random numbers in a given range using the `random.randint()` function.
import random

# Pygame is used to load and play the music used for the game!
import pygame

# The Randfacts library is for generating random facts after every milestone.
import randfacts

# Variables

# The amount of time it takes to load the start text, this can be changed to adjust the time it takes.
rules_pop_up = 2.0

# This is music used for the game, you add your own music files into the program's directory and change this variable.
midi = 'Various Artists - Double Impact MIDI Pack - E1M6, E4M6 - ZeMystic - Andromeda.mid'

# The volume of the music being played.
volume = 0.5

# Will the music repeat? It's set to loop infinitely by default.
loop = -1

# The start time of the music, change this if want the music to start in a different place.
start_position = 0.0

# This is if you want the music to fade out, it is disabled by default.
fade = 0

# These three variables are for the player to place their input
start_game = ""
answers = ""
continue_game = ""

# The time limit for player to answer each question, this can be adjusted for longer or shorter times.
seconds = 3

# These variables are range of numbers being chosen in the `random.randint()` function, change if you want a different range of numbers chosen.
numbers_1 = 0
numbers_2 = 100000000000000000000000000000000000000000000000000000000000000000000000000

# Main Function
def main():
    
    # Adding Music
    pygame.init()

    # Loading Music
    pygame.mixer.music.load(midi)

    # Music Volume
    pygame.mixer.music.set_volume(volume)

    # Playing Music
    pygame.mixer.music.play(loops=loop, start=start_position, fade_ms=fade)

    # Start Text and Rules
    print("░█▀▀░█▀▀░█▀█░█▀▄░█▀▀░░░█▀▀░█░░░▀█▀░█▀▄░█▀▀░█▀▄░█\n ▀▀█░█░░░█░█░█▀▄░█▀▀░░░█░█░█░░░░█░░█░█░█▀▀░█▀▄░▀\n░▀▀▀░▀▀▀░▀▀▀░▀░▀░▀▀▀░░░▀▀▀░▀▀▀░▀▀▀░▀▀░░▀▀▀░▀░▀░▀\n")
    time.sleep(rules_pop_up)
    print("Rules:\n")
    print("1. The game is about an infinite amount of random numbers being shown to the player and the player needs to guess if they are even or odd.\n")
    print("2. If the player doesn't answer within", seconds, "seconds, the game is over.\n")
    print("3. If the player pauses the game, the player is given another number.\n")
    print("4. As you get correct answers, your score builds up leading to milestones (new personal bests!) that reveals random facts! \n")
    print("5. This game can be played with one player or many players.\n")
    print("Key binds: Enter 'o' for an odd number, enter 'e' for an even number, enter 'p' to pause the game, and enter 'q' to quit the game in the terminal or console.")

    # Start Game Input
    start_game = input("\nStart Game? (y/n) ")

    # Score Variable
    score = 0
    
    # When the game is started, making a while loop
    if start_game == "y":  
        while True:

            # Added score and milestone array
            score_added = 5
            milestones = [50, 100, 200, 400, 600, 1000, 1200, 1600, 1800, 3000]

            # Random Number variable changes every time it is added in the code
            num = random.randint(numbers_1,numbers_2)

            # Shows the number to the player after number change
            print(num)

            # Time limit for players to answer the question and gives a times up message, giving a game over message, breaking the while loop and exiting the program.
            t = Timer(seconds, lambda: print(
    "\n\nYour time is up!\n\nPress enter to exit the game"))

            # Starts the Timer
            t.start()

            # The Players' Answer Input
            answers = input("\nIs this number even or odd? ")

            # Even Numbers Input: This is if the number given is even, if the player enters "e" it adds points giving a "That's Correct!" message along with a total score message because the score and score_added is being added together to increase the score variable. If the player doesn't put inputs "e", "q", or "p" the player is given a game over message breaking the loop and ending the program.
            if num % 2 == 0:

                if answers == "e":
                    num = random.randint(numbers_1,numbers_2)
                    print("\nThat's Correct!")
                    score += score_added
                    print("\nYou have", score, "points!")
                    t.cancel()

                if answers != "e" and answers != "q" and answers != "p":
                    print("\nGAME OVER")
                    break

            # Odd Numbers Input: This is if the number given is odd, if the player enters "e" it adds points giving a "That's Correct!" message along with a total score message because the score and score_added is being added together to increase the score variable. If the player doesn't put inputs "e", "q", or "p" the player is given a game over message breaking the loop and ending the program.
            else:

                if answers == "o":
                    num = random.randint(numbers_1,numbers_2)
                    print("\nThat's Correct!")
                    score += score_added
                    print("\nYou have", score, "points!")
                    t.cancel()

                if answers != "o" and answers != "q" and answers != "p":
                    print("\nGAME OVER")
                    break

            # Input to quit the game: gives quitting message to the player and breaks the while loop, ending the program.
            if answers == "q":
                print("\nQuitting...")
                break

            # Input to pause the game: Pauses timer and provides input to continue.
            if answers == "p":
                num = random.randint(numbers_1,numbers_2)
                t.cancel()
                continue_game = input("\nGame Paused, press enter to continue.")

            # Scoring system: For every milestone reached within the `milestones` array a random fact is given to the player, canceling the timer and pausing the game, providing input for the player to continue. Players have to press enter to continue. If they reach the final number in the milestone array, the timer is stopped, a fact is given, and they get a winning message, ending the game.
            if score in milestones:
                t.cancel()
                fact = randfacts.get_fact()
                print("\nRandom Fact:",fact)
                continue_game = input("\nGame Paused, press enter to continue.")

            if score == milestones[-1]:
                t.cancel()
                fact = randfacts.get_fact()
                print("\nRandom Fact:",fact)
                print("\nYou've made it! You have won, congrats!")
                break

    else:

        # Input if player doesn't want to start the game
        print("\nQuitting...")

# Function call to launch the program!
main()

# This program is under the BSD-3-Clause License
#
#Copyright 2024 Sensitive-Sky
#
#Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
#
#1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
#
#2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
#
#3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
#
#THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS “AS IS” AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
# The music used is "Andromeda" by ZeMystic in the "Double Impact Midi Pack" is under the CC-BY 3.0 https://creativecommons.org/licenses/by/3.0/ and is available here https://ivanastan.bandcamp.com/track/andromeda
